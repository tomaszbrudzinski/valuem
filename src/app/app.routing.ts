import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'clients', pathMatch: 'full' },
  { path: 'clients', loadChildren: './clients/clients.module#ClientsModule'},
  { path: 'projects', loadChildren: './projects/projects.module#ProjectsModule'},
  { path: 'valuations', loadChildren: './valuations/valuations.module#ValuationsModule'},
  { path: 'settings', loadChildren: './settings/settings.module#SettingsModule'},
  {path: '**', redirectTo: 'clients', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRouting {
}
