import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatDatepickerModule,
  MatPaginatorModule,
  MatDialogModule,
  MatTableModule,
  MatSortModule,
  MatStepperModule,
  MatIconModule, MatTooltipModule
} from '@angular/material';

import {FlexLayoutModule} from '@angular/flex-layout';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    RouterModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatStepperModule,
    FlexLayoutModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatStepperModule,
    FlexLayoutModule,
    MatIconModule,
    MatTooltipModule
  ]
})
export class ThemeModule {

}
