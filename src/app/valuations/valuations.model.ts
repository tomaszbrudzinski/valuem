import {Currency} from '../settings/settings.model';

export enum ValuationType {
  MIXED = 'MIXED',
  AGILE = 'AGILE',
  COCOMO = 'COCOMO'
}

export enum ComplexityType {
  SIMPLE = 'SIMPLE',
  MEDIUM = 'MEDIUM',
  COMPLEX = 'COMPLEX'
}

export interface GeneralSystemCharacteristics {
  dataCommunication: number;
  distributedDataProcessing: number;
  performance: number;
  heavilyUsedConfiguration: number;
  transactionRole: number;
  onlineDataEntry: number;
  endUserEfficiency: number;
  onlineUpdate: number;
  complexProcessing: number;
  reusability: number;
  installationEase: number;
  operationalEase: number;
  multipleSites: number;
  facilitateChange: number;
}

export interface Milestone {
  _id: string;
  name: string;
  description: string;
  duration: number;
  budget: number;
}

export interface Task {
  _id: string;
  name: string;
  milestone: number;
  complexity?: ComplexityType;
  inputs?: number;
  outputs?: number;
  inquiries?: number;
  files?: number;
  interfaces?: number;
  workloadMin?: number;
  workloadMax?: number;
}

export interface Cost {
  _id: string;
  name: string;
  milestone: number;
  value: number;
}

export interface Checkpoint {
  _id: string;
  date: Date;
  bcwp: number;
  acwp: number;
}

export interface Valuation {
  _id: string;
  projectId: string;
  name: string;
  type: ValuationType;
  info?: string;
  currency: Currency;
  complexity: ComplexityType;
  targetBudget?: number;
  manDayCost: number;
  chanceFactor: number;
  riskFactor: number;
  locsPerFunctionPoint: number;
  generalSystemCharacteristics: GeneralSystemCharacteristics;
  startDate: Date;
  createDate: Date;
  updateDate: Date;
  costs: Cost[];
  milestones: Milestone[];
  tasks: Task[];
  checkpoints: Checkpoint[];
}
