import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

import {Valuation} from './valuations.model';
import {Observable} from 'rxjs';

const BACKEND_URL = environment.apiUrl + '/valuations/';

@Injectable({providedIn: 'root'})
export class ValuationsService {

  constructor(private http: HttpClient, private router: Router) {
  }

  private valuations: Valuation[] = [];

  getValuation(valuationId: string): Observable<Valuation> {
    return this.http.get<Valuation>(BACKEND_URL + valuationId);
  }

  createValuation(valuationData: Valuation) {
    valuationData._id = null;
    valuationData.createDate = null;
    valuationData.updateDate = null;
    console.log(valuationData);
    this.http
      .post<{ message: string, valuation: Valuation}>(BACKEND_URL, valuationData)
      .subscribe((responseData) => {
        this.router.navigate(['/projects/' + valuationData.projectId + '/details']);
      });
  }

  updateValuation(valuationData: Valuation) {
    valuationData.createDate = null;
    valuationData.updateDate = null;
    this.http.put(BACKEND_URL + valuationData._id, valuationData)
      .subscribe(response => {
        this.router.navigate(['/projects/' + valuationData.projectId + '/details']);
      });
  }

  deleteValuation(valuationId: string) {
    return this.http.delete(BACKEND_URL + valuationId);
  }
}
