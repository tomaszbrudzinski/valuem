import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ValuationComponent} from './valuation/valuation.component';

const routes: Routes = [
  { path: '', redirectTo: '/clients', pathMatch: 'full'},
  { path: 'create', component: ValuationComponent },
  { path: ':valuationId/details', component: ValuationComponent},
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ValuationsRouting {}
