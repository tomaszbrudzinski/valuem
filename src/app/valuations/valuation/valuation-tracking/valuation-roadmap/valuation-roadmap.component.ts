import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Valuation, ValuationType} from '../../../valuations.model';
import {MatTable} from '@angular/material';

@Component({
  selector: 'app-valuation-roadmap',
  templateUrl: './valuation-roadmap.component.html',
  styleUrls: ['./valuation-roadmap.component.scss']
})
export class ValuationRoadmapComponent implements OnInit {

  displayedColumns = ['number', 'color', 'name', 'duration', 'budget', 'hints'];
  startDate: Date;
  private projectBudgetValue: number;
  @Input() valuation: Valuation;
  @Input() milestonesForm: FormGroup;
  @Input() chromaScale: [];
  @Output() roadmapUpdated = new EventEmitter<null>();
  @ViewChild('milestonesTable') milestonesTable: MatTable<any>;

  constructor(private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
  }

  updateRoadmap() {
    this.roadmapUpdated.emit();
  }

  renderTable() {
    this.milestonesTable.renderRows();
  }

}
