import {Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Valuation, ValuationType} from '../../../valuations.model';
import {MatTable} from '@angular/material';

@Component({
  selector: 'app-valuation-checkpoints',
  templateUrl: './valuation-checkpoints.component.html',
  styleUrls: ['./valuation-checkpoints.component.scss']
})
export class ValuationCheckpointsComponent implements OnInit {

  displayedColumns = ['number', 'date', 'bcwp', 'acwp', 'cpi', 'actions'];
  @Input() valuation: Valuation;
  @Input() checkpointsForm: FormGroup;
  @Output() checkpointAdded = new EventEmitter<number>();
  @Output() checkpointRemoved = new EventEmitter<number>();
  @Output() checkpointUpdated = new EventEmitter<number>();
  @ViewChild('checkpointsTable') checkpointsTable: MatTable<any>;
  @ViewChildren('date') dateInputs: QueryList<ElementRef>;

  constructor() { }

  ngOnInit() {
  }

  addCheckpointAtPosition(event, rowIndex: number) {
    if (event.key === 'Enter') {
      this.checkpointAdded.emit(rowIndex);
      this.checkpointsTable.renderRows();
      setTimeout(() => {
        this.dateInputs.find(element => {
          if (element.nativeElement.id === 'date' + (rowIndex + 1)) {
            element.nativeElement.focus();
            return true;
          }
          return false;
        });
      }, 100);
    }
  }

  addCheckpoint() {
    this.checkpointAdded.emit(this.checkpointsForm.get('checkpoints')['controls'].length);
    this.checkpointsTable.renderRows();
  }

  removeCheckpoint(rowIndex: number) {
    this.checkpointRemoved.emit(rowIndex);
    this.checkpointsTable.renderRows();
  }

  updateCheckpoint() {
    this.checkpointUpdated.emit();
  }

}
