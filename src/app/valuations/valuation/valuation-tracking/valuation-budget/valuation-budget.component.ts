import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Valuation} from '../../../valuations.model';
import {map} from 'rxjs/operators';
import * as shape from 'd3-shape';
import * as moment from 'moment';

@Component({
  selector: 'app-valuation-budget',
  templateUrl: './valuation-budget.component.html',
  styleUrls: ['./valuation-budget.component.scss']
})
export class ValuationBudgetComponent implements OnInit, OnChanges {

  isDataReady = false;
  chartData: any[];
  chartColorScheme = {domain: ['#5caf4d', '#ef4747', '#4286f4', '#e59595']};
  chartInterpolation = shape.curveCatmullRom.alpha(0.5);
  projectEndDate: Date;
  bac = 0;
  eac = 0;
  @Input() valuation: Valuation;

  constructor() {
  }

  ngOnInit() {
    this.renderChart();
  }

  renderChart() {
    let projectPhaseDuration = 0;
    let projectPhaseBudget = 0;
    let projectStartDate = moment();
    let lastAcwp: number;
    let lastBcwp: number;

    let bacReady = false;
    let checkpointsReady = false;

    this.chartData = [];

    if (this.valuation.startDate) {
      projectStartDate = moment(this.valuation.startDate.toISOString());
    }

    this.projectEndDate = projectStartDate.toDate();
    // BAC

    const bacSeries = this.valuation.milestones.map(milestone => {
      projectPhaseDuration += milestone.duration;
      const milestoneDate = moment(projectStartDate.toDate());
      milestoneDate.add(projectPhaseDuration, 'day');
      this.projectEndDate = milestoneDate.toDate();
      this.bac += milestone.budget;
      if (milestone.duration >= 1 && milestone.budget >= 1) {
        bacReady = true;
      }
      return {
        name: milestoneDate.toDate(),
        value: projectPhaseBudget += milestone.budget
      };
    });
    if (bacReady) {
      bacSeries.splice(0, 0, {
        name: projectStartDate.toDate(),
        value: 0
      });
      this.chartData.push({
        name: 'BAC',
        series: bacSeries
      });
    }

    // ACWP

    const acwpSeries = this.valuation.checkpoints.map(checkpoint => {
      if (checkpoint.date && +checkpoint.acwp > 0 && checkpoint.bcwp) {
        checkpointsReady = true;
        const checkpointDate = new Date(checkpoint.date.toString());
        lastAcwp = checkpoint.acwp;
        lastBcwp = checkpoint.bcwp;
        return {
          name: checkpointDate,
          value: checkpoint.acwp
        };
      }
    });
    if (checkpointsReady) {
      acwpSeries.splice(0, 0, {
        name: projectStartDate.toDate(),
        value: 0
      });
      this.chartData.push({
        name: 'ACWP',
        series: acwpSeries
      });
    }

    // BCWP

    const bcwpSeries = this.valuation.checkpoints.map(checkpoint => {
      if (checkpoint.date && checkpoint.acwp > 0 && checkpoint.bcwp) {
        const checkpointDate = new Date(checkpoint.date.toString());
        return {
          name: checkpointDate,
          value: checkpoint.bcwp
        };
      }
    });
    if (checkpointsReady) {
      bcwpSeries.splice(0, 0, {
        name: projectStartDate.toDate(),
        value: 0
      });
      this.chartData.push({
        name: 'BCWP',
        series: bcwpSeries
      });
    }


    if (checkpointsReady) {
      const eacSeries = [...acwpSeries];

      eacSeries.push({
        name: this.projectEndDate,
        value: lastAcwp + ((this.bac - lastBcwp) / (lastBcwp / lastAcwp))
      });

      this.chartData.push({
        name: 'EAC',
        series: eacSeries
      });
    }

    if (bacReady) {
      this.isDataReady = true;
    } else {
      this.isDataReady = false;
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.renderChart();
  }
}
