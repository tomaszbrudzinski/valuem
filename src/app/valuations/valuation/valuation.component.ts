import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {forkJoin} from 'rxjs';
import * as chroma from 'chroma-js';

import {ValuationsService} from '../valuations.service';
import {ComplexityType, Milestone, Valuation, ValuationType, Task, Cost, Checkpoint} from '../valuations.model';
import {ProjectsService} from '../../projects/projects.service';
import {Project} from '../../projects/projects.model';
import {ClientsService} from '../../clients/clients.service';
import {Client} from '../../clients/clients.model';
import {ValuationTasksComponent} from './valuation-scope/valuation-tasks/valuation-tasks.component';
import {ValuationRoadmapComponent} from './valuation-tracking/valuation-roadmap/valuation-roadmap.component';
import {Currency, Settings} from '../../settings/settings.model';
import {SettingsService} from '../../settings/settings.service';

@Component({
  selector: 'app-valuation',
  templateUrl: './valuation.component.html',
  styleUrls: ['./valuation.component.scss']
})
export class ValuationComponent implements OnInit {

  ValuationTypes = ValuationType; // enum used in the template
  project: Project;
  client: Client;
  isLoading = false;
  basicsForm: FormGroup;
  milestonesForm: FormGroup;
  tasksForm: FormGroup;
  costsForm: FormGroup;
  assumptionsForm: FormGroup;
  checkpointsForm: FormGroup;
  defaultSettings: Settings;
  currentStep: number;
  chromaScale: [];
  @ViewChild(ValuationTasksComponent) valuationTasksComponent;
  @ViewChild(ValuationRoadmapComponent) valuationRoadmapComponent;
  private mode = 'create';
  private valuationId: string;
  private projectId: string;
  private clientId: string;

  // TODO: Get defaults from settings
  valuation: Valuation = {
    _id: '',
    projectId: '',
    name: '',
    type: ValuationType.MIXED,
    info: '',
    currency: Currency.PLN,
    complexity: null,
    targetBudget: 0,
    manDayCost: 0,
    chanceFactor: 0.9,
    riskFactor: 1.5,
    locsPerFunctionPoint: 0,
    generalSystemCharacteristics: {
      dataCommunication: 0,
      distributedDataProcessing: 0,
      performance: 0,
      heavilyUsedConfiguration: 0,
      transactionRole: 0,
      onlineDataEntry: 0,
      endUserEfficiency: 0,
      onlineUpdate: 0,
      complexProcessing: 0,
      reusability: 0,
      installationEase: 0,
      operationalEase: 0,
      multipleSites: 0,
      facilitateChange: 0,
    },
    startDate: new Date(),
    createDate: null,
    updateDate: null,
    costs: [],
    milestones: [],
    tasks: [],
    checkpoints: []
  };

  constructor(
    public valuationsService: ValuationsService,
    public route: ActivatedRoute,
    private projectsService: ProjectsService,
    private clientsService: ClientsService,
    private settingsService: SettingsService,
    private router: Router,
    private formBuilder: FormBuilder,
    private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit() {

    this.isLoading = true;

    // BASICS FORM
    this.basicsForm = new FormGroup({
      name: new FormControl(this.valuation.name, {validators: [Validators.required, Validators.minLength(3)]}),
      info: new FormControl(this.valuation.info),
      type: new FormControl(this.valuation.type),
      currency: new FormControl(this.valuation.currency)
    });

    // MILESTONES FORM
    this.milestonesForm = this.formBuilder.group({
      milestones: this.formBuilder.array([this.initMilestoneRow()])
    });

    // TASKS FORM
    this.tasksForm = this.formBuilder.group({
      tasks: this.formBuilder.array([this.initTaskRow()])
    });

    // COSTS FORM
    this.costsForm = this.formBuilder.group({
      costs: this.formBuilder.array([])
    });

    // ASSUMPTIONS FORM
    this.assumptionsForm = new FormGroup({
      complexity: new FormControl(this.valuation.complexity, {validators: [Validators.required]}),
      manDayCost: new FormControl(this.valuation.manDayCost, {validators: [Validators.required, Validators.min(1)]}),
      targetBudget: new FormControl(this.valuation.targetBudget, {validators: [Validators.required, Validators.min(1)]}),
      chanceFactor: new FormControl(this.valuation.chanceFactor, {validators: [Validators.min(0.1)]}),
      riskFactor: new FormControl(this.valuation.riskFactor, {validators: [Validators.min(1)]}),
      locsPerFunctionPoint: new FormControl(this.valuation.locsPerFunctionPoint, {validators: [Validators.min(1)]}),
      generalSystemCharacteristics: new FormGroup({
        dataCommunication: new FormControl(this.valuation.generalSystemCharacteristics.dataCommunication, {validators: [Validators.min(0), Validators.max(5)]}),
        distributedDataProcessing: new FormControl(this.valuation.generalSystemCharacteristics.distributedDataProcessing, {validators: [Validators.min(0), Validators.max(5)]}),
        performance: new FormControl(this.valuation.generalSystemCharacteristics.performance, {validators: [Validators.min(0), Validators.max(5)]}),
        heavilyUsedConfiguration: new FormControl(this.valuation.generalSystemCharacteristics.heavilyUsedConfiguration, {validators: [Validators.min(0), Validators.max(5)]}),
        transactionRole: new FormControl(this.valuation.generalSystemCharacteristics.transactionRole, {validators: [Validators.min(0), Validators.max(5)]}),
        onlineDataEntry: new FormControl(this.valuation.generalSystemCharacteristics.onlineDataEntry, {validators: [Validators.min(0), Validators.max(5)]}),
        endUserEfficiency: new FormControl(this.valuation.generalSystemCharacteristics.endUserEfficiency, {validators: [Validators.min(0), Validators.max(5)]}),
        onlineUpdate: new FormControl(this.valuation.generalSystemCharacteristics.onlineUpdate, {validators: [Validators.min(0), Validators.max(5)]}),
        complexProcessing: new FormControl(this.valuation.generalSystemCharacteristics.complexProcessing, {validators: [Validators.min(0), Validators.max(5)]}),
        reusability: new FormControl(this.valuation.generalSystemCharacteristics.reusability, {validators: [Validators.min(0), Validators.max(5)]}),
        installationEase: new FormControl(this.valuation.generalSystemCharacteristics.installationEase, {validators: [Validators.min(0), Validators.max(5)]}),
        operationalEase: new FormControl(this.valuation.generalSystemCharacteristics.operationalEase, {validators: [Validators.min(0), Validators.max(5)]}),
        multipleSites: new FormControl(this.valuation.generalSystemCharacteristics.multipleSites, {validators: [Validators.min(0), Validators.max(5)]}),
        facilitateChange: new FormControl(this.valuation.generalSystemCharacteristics.facilitateChange, {validators: [Validators.min(0), Validators.max(5)]}),
      })
    });

    // CHECKPOINTS FORM

    this.checkpointsForm = this.formBuilder.group({
      checkpoints: this.formBuilder.array([])
    });

    // SET DEFAULT FORM VALUES

    this.settingsService.getSettings().then(settings => {
      this.defaultSettings = settings;
      this.basicsForm.get('currency').setValue(this.defaultSettings.defaultCurrency);
      this.assumptionsForm.get('manDayCost').setValue(this.defaultSettings.defaultManDayCost);
      this.assumptionsForm.get('chanceFactor').setValue(this.defaultSettings.defaultChanceFactor);
      this.assumptionsForm.get('riskFactor').setValue(this.defaultSettings.defaultRiskFactor);
      this.assumptionsForm.get('locsPerFunctionPoint').setValue(this.defaultSettings.defaultLocsPerFunctionPoint);
    });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {

      // get valuation contexts

      this.valuation.projectId = ProjectsService.getProjectContext();
      this.clientId = ClientsService.getClientContext();

      // create new valuation

      if (this.valuation.projectId && this.clientId) {

        if (this.router.url.includes('create')) {

          this.currentStep = 0;
          this.mode = 'create';
          this.valuationId = null;

          forkJoin(
            this.projectsService.getProject(this.valuation.projectId),
            this.clientsService.getClient(this.clientId)
          ).pipe(
            map(([projectData, clientData]) => {
              return {
                projectData,
                clientData
              };
            })
          ).subscribe((combinedData) => {
            this.project = combinedData.projectData;
            this.client = combinedData.clientData;
            this.isLoading = false;
            this.setChromaScale(this.valuation.milestones.length);
          });

          // edit existing valuation

        } else if (paramMap.has('valuationId')) {

          this.currentStep = 3;
          this.mode = 'edit';
          this.valuationId = paramMap.get('valuationId');

          forkJoin(
            this.valuationsService.getValuation(this.valuationId),
            this.projectsService.getProject(this.valuation.projectId),
            this.clientsService.getClient(this.clientId)
          ).pipe(
            map(([valuationData, projectData, clientData]) => {
              return {
                valuationData,
                projectData,
                clientData
              };
            })
          ).subscribe((combinedData) => {
            this.valuation = {
              ...combinedData.valuationData,
              startDate: new Date(combinedData.valuationData.startDate),
              createDate: new Date(combinedData.valuationData.createDate),
              updateDate: new Date(combinedData.valuationData.updateDate),
            };
            this.project = combinedData.projectData;
            this.client = combinedData.clientData;
            this.basicsForm.setValue({
              name: this.valuation.name,
              info: this.valuation.info,
              type: this.valuation.type,
              currency: this.valuation.currency
            });
            if (this.valuation.milestones.length > 0) {
              this.milestonesForm.setControl('milestones', this.populateMilestonesForm(this.valuation.milestones));
            }
            if (this.valuation.tasks.length > 0) {
              this.tasksForm.setControl('tasks', this.populateTasksForm(this.valuation.tasks));
            }
            if (this.valuation.costs.length > 0) {
              this.costsForm.setControl('costs', this.populateCostsForm(this.valuation.costs));
            }
            this.assumptionsForm.setValue({
              complexity: this.valuation.complexity,
              manDayCost: this.valuation.manDayCost,
              targetBudget: this.valuation.targetBudget,
              chanceFactor: this.valuation.chanceFactor,
              riskFactor: this.valuation.riskFactor,
              locsPerFunctionPoint: this.valuation.locsPerFunctionPoint,
              generalSystemCharacteristics: {
                dataCommunication: this.valuation.generalSystemCharacteristics.dataCommunication,
                distributedDataProcessing: this.valuation.generalSystemCharacteristics.distributedDataProcessing,
                performance: this.valuation.generalSystemCharacteristics.performance,
                heavilyUsedConfiguration: this.valuation.generalSystemCharacteristics.heavilyUsedConfiguration,
                transactionRole: this.valuation.generalSystemCharacteristics.transactionRole,
                onlineDataEntry: this.valuation.generalSystemCharacteristics.onlineDataEntry,
                endUserEfficiency: this.valuation.generalSystemCharacteristics.endUserEfficiency,
                onlineUpdate: this.valuation.generalSystemCharacteristics.onlineUpdate,
                complexProcessing: this.valuation.generalSystemCharacteristics.complexProcessing,
                reusability: this.valuation.generalSystemCharacteristics.reusability,
                installationEase: this.valuation.generalSystemCharacteristics.installationEase,
                operationalEase: this.valuation.generalSystemCharacteristics.operationalEase,
                multipleSites: this.valuation.generalSystemCharacteristics.multipleSites,
                facilitateChange: this.valuation.generalSystemCharacteristics.facilitateChange
              }
            });
            if (this.valuation.checkpoints.length > 0) {
              this.checkpointsForm.setControl('checkpoints', this.populateCheckpointsForm(this.valuation.checkpoints));
            }
            this.isLoading = false;
            this.setChromaScale(this.valuation.milestones.length);
          });
        } else {
          this.redirectToProjectOrClients();
        }
      } else {
        this.redirectToProjectOrClients();
      }
    });
  }

  redirectToProjectOrClients() {
    if (this.valuation.projectId) {
      this.router.navigate(['/projects/' + this.valuation.projectId]);
    } else {
      this.router.navigate(['/']);
    }
  }

  changeValuationType(type: ValuationType) {
    this.changeDetector.detectChanges();
    this.valuationTasksComponent.renderTable();
  }

  setChromaScale(scaleLength) {
    // const spectralInversed = ['#5e4fa2', '#3288bd', '#66c2a5', '#abdda4', '#e6f598', '#ffffbf', '#fee08b', '#fdae61', '#f46d43', '#d53e4f', '#9e0142'];
    const spectralInversed = ['#5e4fa2', '#3288bd', '#56af93', '#85cf7b', '#ccdf6a', '#fbe090', '#faca64', '#ec9b4d', '#f46d43', '#d53e4f', '#9e0142'];

    this.chromaScale = chroma.scale(spectralInversed).padding(0.10).colors(scaleLength);
  }

  // FORMS UTILS //////////////////////////////////////////////////////////////////////////////////////

  // MILESTONES

  initMilestoneRow(): FormGroup {
    return this.formBuilder.group({
      name: new FormControl('', {validators: [Validators.required]}),
      description: new FormControl(''),
      duration: new FormControl(0),
      budget: new FormControl(0)
    });
  }

  populateMilestonesForm(milestones: Milestone[]): FormArray {
    const milestonesArray = new FormArray([]);
    milestones.forEach(milestone => {
      milestonesArray.push(
        this.formBuilder.group({
          name: new FormControl(milestone.name, {validators: [Validators.required]}),
          description: new FormControl(milestone.description),
          duration: new FormControl(milestone.duration),
          budget: new FormControl(milestone.budget)
        })
      );
    });
    return milestonesArray;
  }

  addMilestoneRow(rowIndex?: number) {
    const milestones = this.milestonesForm.get('milestones')['controls'];
    milestones.splice(rowIndex + 1, 0, this.initMilestoneRow());
    this.setChromaScale(milestones.length);
  }

  removeMilestoneRow(rowIndex: number): void {
    const milestones = this.milestonesForm.get('milestones')['controls'];
    if (milestones.length > 1) {
      milestones.splice(rowIndex, 1);
      this.setChromaScale(milestones.length);
    }
  }

  // TASKS

  initTaskRow(): FormGroup {
    return this.formBuilder.group({
      name: new FormControl('', {validators: [Validators.required]}),
      milestone: new FormControl(0, {validators: [Validators.required]}),
      complexity: new FormControl(ComplexityType.SIMPLE, {validators: [Validators.required]}),
      inputs: new FormControl(0, {validators: [Validators.min(0)]}),
      outputs: new FormControl(0, {validators: [Validators.min(0)]}),
      inquiries: new FormControl(0, {validators: [Validators.min(0)]}),
      files: new FormControl(0, {validators: [Validators.min(0)]}),
      interfaces: new FormControl(0, {validators: [Validators.min(0)]}),
      workloadMin: new FormControl(0, {validators: [Validators.min(0)]}),
      workloadMax: new FormControl(0, {validators: [Validators.min(0)]})
    });
  }

  populateTasksForm(tasks: Task[]): FormArray {
    const tasksArray = new FormArray([]);
    tasks.forEach(task => {
      tasksArray.push(
        this.formBuilder.group({
          name: new FormControl(task.name, {validators: [Validators.required]}),
          milestone: new FormControl(task.milestone, {validators: [Validators.required]}),
          complexity: new FormControl(task.complexity, {validators: [Validators.required]}),
          inputs: new FormControl(task.inputs, {validators: [Validators.min(0)]}),
          outputs: new FormControl(task.outputs, {validators: [Validators.min(0)]}),
          inquiries: new FormControl(task.inquiries, {validators: [Validators.min(0)]}),
          files: new FormControl(task.files, {validators: [Validators.min(0)]}),
          interfaces: new FormControl(task.interfaces, {validators: [Validators.min(0)]}),
          workloadMin: new FormControl(task.workloadMin, {validators: [Validators.min(0)]}),
          workloadMax: new FormControl(task.workloadMax, {validators: [Validators.min(0)]})
        })
      );
    });
    return tasksArray;
  }

  addTaskRow(rowIndex?: number) {
    const tasks = this.tasksForm.get('tasks')['controls'];
    tasks.splice(rowIndex + 1, 0, this.initTaskRow());
  }

  removeTaskRow(rowIndex: number): void {
    const tasks = this.tasksForm.get('tasks')['controls'];
    if (tasks.length > 1) {
      tasks.splice(rowIndex, 1);
    }
  }

  // COSTS

  initCostsRow(): FormGroup {
    return this.formBuilder.group({
      name: new FormControl('', {validators: [Validators.required]}),
      milestone: new FormControl(0, {validators: [Validators.required]}),
      value: new FormControl(0, {validators: [Validators.required, Validators.min(1)]})
    });
  }

  populateCostsForm(costs: Cost[]): FormArray {
    const costsArray = new FormArray([]);
    costs.forEach(cost => {
      costsArray.push(
        this.formBuilder.group({
          name: new FormControl(cost.name, {validators: [Validators.required]}),
          milestone: new FormControl(cost.milestone, {validators: [Validators.required]}),
          value: new FormControl(cost.value, {validators: [Validators.required, Validators.min(1)]})
        })
      );
    });
    return costsArray;
  }

  addCostsRow(rowIndex?: number) {
    const costs = this.costsForm.get('costs')['controls'];
    costs.splice(rowIndex + 1, 0, this.initCostsRow());
  }

  removeCostsRow(rowIndex: number): void {
    const costs = this.costsForm.get('costs')['controls'];
    if (costs.length > 1) {
      costs.splice(rowIndex, 1);
    }
  }

  // CHECKPOINTS

  initCheckpointRow(): FormGroup {
    return this.formBuilder.group({
      date: new FormControl(null, {validators: [Validators.required]}),
      bcwp: new FormControl(0, {validators: [Validators.required, Validators.min(0)]}),
      acwp: new FormControl(0, {validators: [Validators.required, Validators.min(1)]}),
    });
  }

  populateCheckpointsForm(checkpoints: Checkpoint[]): FormArray {
    const checkpointsArray = new FormArray([]);
    checkpoints.forEach(checkpoint => {
      checkpointsArray.push(
        this.formBuilder.group({
          date: new FormControl(checkpoint.date, {validators: [Validators.required]}),
          bcwp: new FormControl(checkpoint.bcwp, {validators: [Validators.required, Validators.min(1)]}),
          acwp: new FormControl(checkpoint.acwp, {validators: [Validators.required, Validators.min(1)]})
        })
      );
    });
    return checkpointsArray;
  }

  addCheckpointRow(rowIndex?: number) {
    const checkpoints = this.checkpointsForm.get('checkpoints')['controls'];
    checkpoints.splice(rowIndex + 1, 0, this.initCheckpointRow());
  }

  removeCheckpointRow(rowIndex: number): void {
    const checkpoints = this.checkpointsForm.get('checkpoints')['controls'];
    if (checkpoints.length > 1) {
      checkpoints.splice(rowIndex, 1);
    }
  }

  // END OF FORMS UTILS ///////////////////////////////////////////////////////////////////////////////

  mapFormsToModel() {
    const basics = this.basicsForm.value;
    const milestoneControls = this.milestonesForm.get('milestones')['controls'];
    const taskControls = this.tasksForm.get('tasks')['controls'];
    const costControls = this.costsForm.get('costs')['controls'];
    const assumptions = this.assumptionsForm.value;
    const checkpointControls = this.checkpointsForm.get('checkpoints')['controls'];

    const milestonesArray = milestoneControls.map(milestoneControlsData => {
      return milestoneControlsData.value;
    });
    const tasksArray = taskControls.map(taskControlsData => {
      return taskControlsData.value;
    });
    const costsArray = costControls.map(costControlsData => {
      return costControlsData.value;
    });
    const checkpointsArray = checkpointControls.map(checkpointControlsData => {
      return checkpointControlsData.value;
    });

    // TODO: default currency should be taken from DB
    this.valuation = {
      ...this.valuation,
      name: basics.name,
      type: basics.type,
      info: basics.info,
      currency: basics.currency,
      complexity: assumptions.complexity,
      targetBudget: assumptions.targetBudget,
      manDayCost: assumptions.manDayCost,
      chanceFactor: assumptions.chanceFactor,
      riskFactor: assumptions.riskFactor,
      locsPerFunctionPoint: assumptions.locsPerFunctionPoint,
      generalSystemCharacteristics: {
        dataCommunication: assumptions.generalSystemCharacteristics.dataCommunication,
        distributedDataProcessing: assumptions.generalSystemCharacteristics.distributedDataProcessing,
        performance: assumptions.generalSystemCharacteristics.performance,
        heavilyUsedConfiguration: assumptions.generalSystemCharacteristics.heavilyUsedConfiguration,
        transactionRole: assumptions.generalSystemCharacteristics.transactionRole,
        onlineDataEntry: assumptions.generalSystemCharacteristics.onlineDataEntry,
        endUserEfficiency: assumptions.generalSystemCharacteristics.endUserEfficiency,
        onlineUpdate: assumptions.generalSystemCharacteristics.onlineUpdate,
        complexProcessing: assumptions.generalSystemCharacteristics.complexProcessing,
        reusability: assumptions.generalSystemCharacteristics.reusability,
        installationEase: assumptions.generalSystemCharacteristics.installationEase,
        operationalEase: assumptions.generalSystemCharacteristics.operationalEase,
        multipleSites: assumptions.generalSystemCharacteristics.multipleSites,
        facilitateChange: assumptions.generalSystemCharacteristics.facilitateChange,
      },
      startDate: this.valuation.startDate,
      createDate: null,
      updateDate: null,
      milestones: milestonesArray,
      costs: costsArray,
      tasks: tasksArray,
      checkpoints: checkpointsArray
    };

    // console.log(this.valuation);
  }

  mapFormsToModelAsync() {
    setTimeout(() => {
      this.mapFormsToModel();
    }, 100);
  }

  nextStep(stepper) {
    this.currentStep++;
    stepper.next();
  }

  previousStep(stepper) {
    this.currentStep--;
    stepper.previous();
  }

  stepChanged(event) {
    this.currentStep = event.selectedIndex;
    this.mapFormsToModel();
    this.valuationRoadmapComponent.renderTable();
  }

  saveValuation() {
    this.mapFormsToModel();
    if (this.mode === 'create') {
      this.valuationsService.createValuation(this.valuation);
    } else {
      this.valuationsService.updateValuation(this.valuation);
    }
  }

  duplicateValuation() {
    this.valuation.name = this.valuation.name + ' duplicate';
    this.valuation._id = null;
    this.valuationsService.createValuation(this.valuation);
  }

  deleteValuation(valuationId: string) {
    if (confirm('Are you sure you want to delete this valuation?')) {
      this.isLoading = true;
      this.valuationsService.deleteValuation(valuationId).subscribe(() => {
        this.router.navigate(['/projects/' + this.project._id + '/details']);
      }, () => {
        this.isLoading = false;
      });
    }
  }

}
