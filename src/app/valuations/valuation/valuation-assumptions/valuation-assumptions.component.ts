import {AfterViewChecked, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ComplexityType, Valuation, ValuationType} from '../../valuations.model';
import {ValuationLocsSuggestionsComponent} from './valuation-locs-suggestions/valuation-locs-suggestions.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-valuation-assumptions',
  templateUrl: './valuation-assumptions.component.html',
  styleUrls: ['./valuation-assumptions.component.scss']
})
export class ValuationAssumptionsComponent implements OnInit, OnChanges, AfterViewChecked {

  ValuationTypes = ValuationType;
  ComplexityTypes = ComplexityType;
  suggestedChanceFactor: number;
  suggestedRiskFactor: number;
  @Input() assumptionsForm: FormGroup;
  @Input() valuationType: ValuationType;
  @Input() valuation: Valuation;
  @ViewChild('locsPerFunctionPoint') locsPerFunctionPoint: ElementRef<HTMLInputElement>;

  constructor(private changeDefector: ChangeDetectorRef, private dialog: MatDialog) { }

  ngOnInit() {
    this.assumptionsForm.get('complexity').valueChanges.subscribe(complexity => {
      switch (complexity) {
        case 'SIMPLE': {
          this.suggestedChanceFactor = 0.85;
          this.suggestedRiskFactor = 1.5;
          break;
        }
        case 'MEDIUM': {
          this.suggestedChanceFactor = 0.9;
          this.suggestedRiskFactor = 1.7;

          break;
        }
        case 'COMPLEX': {
          this.suggestedChanceFactor = 0.95;
          this.suggestedRiskFactor = 1.9;

          break;
        }
      }
    });
    // initializing suggested factors
    this.assumptionsForm.get('complexity').setValue(this.assumptionsForm.get('complexity').value);
  }

  ngOnChanges(): void {

  }

  ngAfterViewChecked(): void {
    this.changeDefector.detectChanges();
  }

  showLocsSuggestions() {
    this.dialog.open(ValuationLocsSuggestionsComponent);
  }

}


