import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ComplexityType, Valuation, ValuationType} from '../../valuations.model';
import * as shape from 'd3-shape';
import * as chroma from 'chroma-js';

@Component({
  selector: 'app-valuation-summary',
  templateUrl: './valuation-summary.component.html',
  styleUrls: ['./valuation-summary.component.scss']
})
export class ValuationSummaryComponent implements OnInit, OnChanges {

  ValuationTypes = ValuationType;
  chartData = [];
  chartColorScheme = {domain: []};
  chartInterpolation = shape.curveCatmullRom.alpha(0.5);
  estimatedWorkloadMin: number;
  estimatedWorkloadMax: number;
  estimatedWorkloadAvg: number;
  costsTotal: number;

  milestonesData: {
    workloadMin: number;
    workloadMax: number;
    totalWorkloadMin: number;
    totalWorkloadMax: number;
    costs: number;
    costsTotal: number;
    inputs: number[];
    outputs: number[];
    inquiries: number[];
    files: number[];
    interfaces: number[];
    targetBudgetLikelilhood: number;
    agileChartDataset: {
      name: number,
      value: number
    }[]
  }[] = [];

  calculatedWorkloadMin: number;
  calculatedWorkloadMax: number;
  calculatedCostMin: number;
  calculatedCostMax: number;

  targetBudgetLikelilhood: number;

  agileBudgetMin: number;
  agileBudgetMax: number;

  inputs: number[] = [0, 0, 0];
  outputs: number[] = [0, 0, 0];
  inquiries: number[] = [0, 0, 0];
  files: number[] = [0, 0, 0];
  interfaces: number[] = [0, 0, 0];

  isChartRenderable = false;


  @Input() valuation: Valuation;
  @Input() chromaScale: [];

  constructor() {
  }

  ngOnInit() {
    this.calculateValuation();
  }

  calculateValuation() {

    // RESET FORM

    this.chartData = [];

    this.milestonesData = [];
    this.inputs = [0, 0, 0];
    this.outputs = [0, 0, 0];
    this.inquiries = [0, 0, 0];
    this.files = [0, 0, 0];
    this.interfaces = [0, 0, 0];

    this.estimatedWorkloadMin = 0;
    this.estimatedWorkloadMax = 0;
    this.estimatedWorkloadAvg = 0;

    this.calculatedWorkloadMin = 0;
    this.calculatedWorkloadMax = 0;
    this.costsTotal = 0;

    this.agileBudgetMin = 0;
    this.agileBudgetMax = 0;

    this.chartColorScheme = {
      domain: this.chromaScale
    };

    for (const milestone of this.valuation.milestones) {
      this.milestonesData.push({
        workloadMin: 0,
        workloadMax: 0,
        totalWorkloadMin: 0,
        totalWorkloadMax: 0,
        costs: 0,
        costsTotal: 0,
        inputs: [0, 0, 0],
        outputs: [0, 0, 0],
        inquiries: [0, 0, 0],
        files: [0, 0, 0],
        interfaces: [0, 0, 0],
        targetBudgetLikelilhood: 0,
        agileChartDataset: []
      });
    }

    // DATA COLLECTION

    this.valuation.tasks.forEach((task, index) => {
      let complexityPosition = 0;
      switch (task.complexity) {
        case ComplexityType.SIMPLE: {
          complexityPosition = 0;
          break;
        }
        case ComplexityType.MEDIUM: {
          complexityPosition = 1;
          break;
        }
        case ComplexityType.COMPLEX: {
          complexityPosition = 2;
          break;
        }
      }

      this.estimatedWorkloadMin += +task.workloadMin;
      this.estimatedWorkloadMax += +task.workloadMax;

      this.milestonesData[task.milestone].workloadMin += task.workloadMin;
      this.milestonesData[task.milestone].workloadMax += task.workloadMax;
      // temporary total information for further calculations
      this.milestonesData[task.milestone].totalWorkloadMin = this.milestonesData[task.milestone].workloadMin;
      this.milestonesData[task.milestone].totalWorkloadMax = this.milestonesData[task.milestone].workloadMax;

      this.milestonesData[task.milestone].inputs[complexityPosition] += task.inputs;
      this.milestonesData[task.milestone].outputs[complexityPosition] += task.outputs;
      this.milestonesData[task.milestone].inquiries[complexityPosition] += task.inquiries;
      this.milestonesData[task.milestone].files[complexityPosition] += task.files;
      this.milestonesData[task.milestone].interfaces[complexityPosition] += task.interfaces;

      this.inputs[complexityPosition] += task.inputs;
      this.outputs[complexityPosition] += task.outputs;
      this.inquiries[complexityPosition] += task.inquiries;
      this.files[complexityPosition] += task.files;
      this.interfaces[complexityPosition] += task.interfaces;
    });

    this.valuation.costs.forEach(cost => {
      this.costsTotal += cost.value;
      this.milestonesData[cost.milestone].costs += cost.value;
      this.milestonesData[cost.milestone].costsTotal += this.milestonesData[cost.milestone].costs;
    });


    // CALCULATIONS

    this.estimatedWorkloadAvg = (this.estimatedWorkloadMin + this.estimatedWorkloadMax) / 2;

    this.calculatedWorkloadMin = Math.round(this.estimatedWorkloadAvg * this.valuation.chanceFactor);
    this.calculatedWorkloadMax = Math.round(this.estimatedWorkloadAvg * this.valuation.riskFactor);

    this.calculatedCostMin = this.calculatedWorkloadMin * this.valuation.manDayCost;
    this.calculatedCostMax = this.calculatedWorkloadMax * this.valuation.manDayCost;

    this.agileBudgetMin = this.calculatedWorkloadMin * this.valuation.manDayCost + this.costsTotal;
    this.agileBudgetMax = this.calculatedWorkloadMax * this.valuation.manDayCost + this.costsTotal;

    // Those values are initialized like this only to be amended in next iterations
    let likelilhoodMin = 100;
    let likelilhoodMax = 0;

    const percentilesCount = 11;

    this.milestonesData.forEach((milestone, index) => {

      if (index > 0) {
        milestone.totalWorkloadMin = this.milestonesData[index - 1].totalWorkloadMin + milestone.workloadMin;
        milestone.totalWorkloadMax = this.milestonesData[index - 1].totalWorkloadMax + milestone.workloadMax;
        milestone.costsTotal = this.milestonesData[index - 1].costsTotal + milestone.costs;
      }

      const estimatedWorkloadDifference = milestone.totalWorkloadMax - milestone.totalWorkloadMin;
      const calculatedWorkloadAverage = (milestone.totalWorkloadMin + milestone.totalWorkloadMax) / 2;
      const calculatedWorkloadDifference = (calculatedWorkloadAverage * this.valuation.riskFactor) - (calculatedWorkloadAverage * this.valuation.chanceFactor);
      const calculatedCostDifference = calculatedWorkloadDifference * this.valuation.manDayCost;
      const percentileStep = calculatedCostDifference / percentilesCount;

      const ratio = ((milestone.totalWorkloadMax * this.valuation.riskFactor) / estimatedWorkloadDifference) * (this.valuation.chanceFactor / this.valuation.riskFactor);

      const center = calculatedCostDifference / 2 - percentileStep / 2 + this.costsTotal;

      for (let i = 0; i < percentilesCount; i++) {
        milestone.agileChartDataset.push({
          name: this.calculatedCostMin + (percentileStep * i) + this.costsTotal,
          value: this.calculateLikelilhood(percentileStep * i, center, ratio * 2)
        });

      }

      const targetBudgetPercentile = this.valuation.targetBudget - this.calculatedCostMin - this.costsTotal;
      milestone.targetBudgetLikelilhood = this.calculateLikelilhood(targetBudgetPercentile, center, ratio * 2);

      if (index === this.milestonesData.length -1) {
        this.targetBudgetLikelilhood = milestone.targetBudgetLikelilhood;
      }

      // Pick smalles likelilhood
      if (likelilhoodMin > milestone.agileChartDataset[0].value) {
        likelilhoodMin = milestone.agileChartDataset[0].value;
      }
      // Pick biggest likelilhood
      if (likelilhoodMax < milestone.agileChartDataset[percentilesCount - 1].value) {
        likelilhoodMax = milestone.agileChartDataset[percentilesCount - 1].value;
      }

      this.chartData.push({
        name: 'M' + (index + 1) + ' - ' + this.valuation.milestones[index].name,
        series: milestone.agileChartDataset
      });

    });

    this.milestonesData.forEach((milestone, index) => {
      milestone.agileChartDataset.splice(0, 1, {
        name: this.calculatedCostMin + this.costsTotal,
        value: likelilhoodMin
      });
      milestone.agileChartDataset.push({
        name: this.calculatedCostMax + this.costsTotal,
        value: likelilhoodMax
      });
    });

    if (this.calculatedWorkloadMin !== this.calculatedWorkloadMax) {
      this.isChartRenderable = true;
    }
  }

  formatPercent = (val) => {
    if (val <= 100) {
      return val.toString() + '%';
    }
  }

  calculateLikelilhood(distanceFromMin: number, center: number, ratio: number) {
    return 31 * Math.atan((distanceFromMin / ((center / ratio))) - ((center) / ((center / ratio)))) + 50;
  }

  colorSuccessScale(percent: number) {
    const color = chroma.scale(['red', 'orange', 'green']);
    return color(percent / 100);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.calculateValuation();
  }
}
