import {Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {MatTable} from '@angular/material';

@Component({
  selector: 'app-valuation-milestones',
  templateUrl: './valuation-milestones.component.html',
  styleUrls: ['./valuation-milestones.component.scss']
})
export class ValuationMilestonesComponent implements OnInit {

  displayedColumns = ['number', 'color', 'name', 'description', 'actions'];
  @Input() milestonesForm: FormGroup;
  @Input() chromaScale: [];
  @Output() milestoneRemoved = new EventEmitter<number>();
  @Output() milestoneAdded = new EventEmitter<number>();
  @ViewChild('milestonesTable') milestonesTable: MatTable<any>;
  @ViewChildren('milestoneName') milestoneNameInputs: QueryList<ElementRef>;

  constructor() {
  }

  ngOnInit() {
  }

  addMilestoneAtPosition(event, rowIndex: number) {
    if (event.key === 'Enter') {
      this.milestoneAdded.emit(rowIndex);
      this.milestonesTable.renderRows();
      setTimeout(() => {
        this.milestoneNameInputs.find(element => {
          if (element.nativeElement.id === 'name' + (rowIndex + 1)) {
            element.nativeElement.focus();
            return true;
          }
          return false;
        });
      }, 100);
    }
  }

  addMilestone() {
    this.milestoneAdded.emit(this.milestonesForm.get('milestones')['controls'].length);
    this.milestonesTable.renderRows();
  }

  removeMilestone(rowIndex: number) {
    this.milestoneRemoved.emit(rowIndex);
    this.milestonesTable.renderRows();
  }

}
