import {Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatTable} from '@angular/material';
import {ComplexityType, ValuationType} from '../../../valuations.model';

@Component({
  selector: 'app-valuation-tasks',
  templateUrl: './valuation-tasks.component.html',
  styleUrls: ['./valuation-tasks.component.scss']
})
export class ValuationTasksComponent implements OnInit {

  ComplexityTypes = ComplexityType; // enum used in the template
  ValuationTypes = ValuationType; // enum used in the template
  displayedColumns = [
    'number',
    'name',
    'color',
    'milestone',
    'complexity',
    'inputs',
    'outputs',
    'inquiries',
    'files',
    'interfaces',
    'workloadMin',
    'workloadMax',
    'actions'
  ];
  @Input() tasksForm: FormGroup;
  @Input() chromaScale: [];
  @Input() milestones: number;
  @Input() valuationType: ValuationType;
  @Output() taskRemoved = new EventEmitter<number>();
  @Output() taskAdded = new EventEmitter<number>();
  @ViewChild('tasksTable') tasksTable: MatTable<any>;
  @ViewChildren('taskName') taskNameInputs: QueryList<ElementRef>;

  constructor() {
  }

  ngOnInit() {
  }

  addTaskAtPosition(event, rowIndex: number) {
    if (event.key === 'Enter') {
      this.taskAdded.emit(rowIndex);
      this.tasksTable.renderRows();
      setTimeout(() => {
        this.taskNameInputs.find(element => {
          if (element.nativeElement.id === 'name' + (rowIndex + 1)) {
            element.nativeElement.focus();
            return true;
          }
          return false;
        });
      }, 100);
    }
  }

  addTask() {
    this.taskAdded.emit(this.tasksForm.get('tasks')['controls'].length);
    this.tasksTable.renderRows();
  }

  removeTask(rowIndex: number) {
    this.taskRemoved.emit(rowIndex);
    this.tasksTable.renderRows();
  }

  renderTable() {
    this.tasksTable.renderRows();
  }

  getDisplayedColumns() {
    switch (this.valuationType) {
      case ValuationType.MIXED: {
        this.displayedColumns = [
          'number',
          'name',
          'color',
          'milestone',
          'complexity',
          'inputs',
          'outputs',
          'inquiries',
          'files',
          'interfaces',
          'workloadMin',
          'workloadMax',
          'actions'
        ];
        break;
      }
      case ValuationType.AGILE: {
        this.displayedColumns = [
          'number',
          'name',
          'color',
          'milestone',
          'workloadMin',
          'workloadMax',
          'actions'
        ];
        break;
      }
      case ValuationType.COCOMO: {
        this.displayedColumns = [
          'number',
          'name',
          'color',
          'milestone',
          'complexity',
          'inputs',
          'outputs',
          'inquiries',
          'files',
          'interfaces',
          'actions'
        ];
        break;
      }
    }

    return this.displayedColumns;
  }

}
