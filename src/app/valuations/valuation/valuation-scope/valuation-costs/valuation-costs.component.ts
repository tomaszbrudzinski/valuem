import {Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {MatTable} from '@angular/material';

@Component({
  selector: 'app-valuation-costs',
  templateUrl: './valuation-costs.component.html',
  styleUrls: ['./valuation-costs.component.scss']
})
export class ValuationCostsComponent implements OnInit {

  displayedColumns = ['number', 'name', 'milestone', 'value', 'actions'];
  @Input() costsForm: FormGroup;
  @Input() milestones: number;
  @Output() costRemoved = new EventEmitter<number>();
  @Output() costAdded = new EventEmitter<number>();
  @ViewChild('costsTable') costsTable: MatTable<any>;
  @ViewChildren('costName') costNameInputs: QueryList<ElementRef>;

  constructor() {
  }

  ngOnInit() {
  }

  addCostAtPosition(event, rowIndex: number) {
    if (event.key === 'Enter') {
      this.costAdded.emit(rowIndex);
      this.costsTable.renderRows();
      setTimeout(() => {
        this.costNameInputs.find(element => {
          if (element.nativeElement.id === 'name' + (rowIndex + 1)) {
            element.nativeElement.focus();
            return true;
          }
          return false;
        });
      }, 100);
    }
  }

  addCost() {
    this.costAdded.emit(this.costsForm.get('costs')['controls'].length);
    this.costsTable.renderRows();
  }

  removeCost(rowIndex: number) {
    this.costRemoved.emit(rowIndex);
    this.costsTable.renderRows();
  }

}
