import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ValuationType} from '../../valuations.model';
import {Currency} from '../../../settings/settings.model';

@Component({
  selector: 'app-valuation-basics',
  templateUrl: './valuation-basics.component.html',
  styleUrls: ['./valuation-basics.component.scss']
})
export class ValuationBasicsComponent implements OnInit {


  ValuationTypes = ValuationType;
  CurrencyTypes = Currency;
  @Input() basicsForm: FormGroup;
  @Output() valuationTypeChanged = new EventEmitter<ValuationType>();

  constructor() { }

  ngOnInit() {
  }

  changeValuationType(event) {
    this.valuationTypeChanged.emit(event.value);
  }

}
