import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import {SumPipe} from './valuation/@pipes/sum.pipe';

import {ThemeModule} from '../@theme/theme.module';
import {ValuationComponent} from './valuation/valuation.component';
import {ValuationsRouting} from './valuations.routing';
import {ValuationBasicsComponent} from './valuation/valuation-basics/valuation-basics.component';
import {ValuationMilestonesComponent} from './valuation/valuation-scope/valuation-milestones/valuation-milestones.component';
import {ValuationTasksComponent} from './valuation/valuation-scope/valuation-tasks/valuation-tasks.component';
import {ValuationCostsComponent} from './valuation/valuation-scope/valuation-costs/valuation-costs.component';
import {ValuationAssumptionsComponent} from './valuation/valuation-assumptions/valuation-assumptions.component';
import {ValuationCheckpointsComponent} from './valuation/valuation-tracking/valuation-checkpoints/valuation-checkpoints.component';
import {ValuationRoadmapComponent} from './valuation/valuation-tracking/valuation-roadmap/valuation-roadmap.component';
import {ValuationBudgetComponent} from './valuation/valuation-tracking/valuation-budget/valuation-budget.component';
import {ValuationSummaryComponent} from './valuation/valuation-summary/valuation-summary.component';
import {ValuationLocsSuggestionsComponent} from './valuation/valuation-assumptions/valuation-locs-suggestions/valuation-locs-suggestions.component';
import {ErrorComponent} from '../error/error.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    RouterModule,
    ValuationsRouting,
    NgxChartsModule
  ],
  declarations: [
    ValuationComponent,
    ValuationBasicsComponent,
    ValuationMilestonesComponent,
    ValuationTasksComponent,
    ValuationCostsComponent,
    ValuationAssumptionsComponent,
    ValuationSummaryComponent,
    ValuationRoadmapComponent,
    ValuationCheckpointsComponent,
    SumPipe,
    ValuationBudgetComponent,
    ValuationLocsSuggestionsComponent
  ],
  entryComponents: [ValuationLocsSuggestionsComponent]
})
export class ValuationsModule {
}

