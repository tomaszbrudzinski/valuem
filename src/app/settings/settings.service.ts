import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

import {Currency, Setting, Settings} from './settings.model';

const BACKEND_URL = environment.apiUrl + '/settings';

@Injectable({providedIn: 'root'})
export class SettingsService {

  private settings: Settings;
  private rawSettings: Setting[];

  constructor(private http: HttpClient, private router: Router) {
  }

  getSettings(): Promise<Settings> {
    return new Promise((resolve, reject) => {
      if (this.settings) {
        resolve(this.settings);
      } else {
        this.http.get<Setting[]>(BACKEND_URL).subscribe(settings => {
          this.rawSettings = settings;
          this.settings = {
            defaultManDayCost: +settings.find(setting => setting.name === 'defaultManDayCost').value,
            defaultChanceFactor: +settings.find(setting => setting.name === 'defaultChanceFactor').value,
            defaultRiskFactor: +settings.find(setting => setting.name === 'defaultRiskFactor').value,
            defaultLocsPerFunctionPoint: +settings.find(setting => setting.name === 'defaultLocsPerFunctionPoint').value,
            defaultCurrency: settings.find(setting => setting.name === 'defaultCurrency').value as Currency,
          };
          resolve(this.settings);
        }, error => {
          reject();
        });
      }
    });
  }

  updateSettings(settings: Settings) {
    this.rawSettings.find(setting => setting.name === 'defaultManDayCost').value = settings.defaultManDayCost;
    this.rawSettings.find(setting => setting.name === 'defaultChanceFactor').value = settings.defaultChanceFactor;
    this.rawSettings.find(setting => setting.name === 'defaultRiskFactor').value = settings.defaultRiskFactor;
    this.rawSettings.find(setting => setting.name === 'defaultLocsPerFunctionPoint').value = settings.defaultLocsPerFunctionPoint;
    this.rawSettings.find(setting => setting.name === 'defaultCurrency').value = settings.defaultCurrency;
    this.http.put(BACKEND_URL, this.settings)
      .subscribe(response => {
        this.router.navigate(['/clients']);
      });
  }
}
