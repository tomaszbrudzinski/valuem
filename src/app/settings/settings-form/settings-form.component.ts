import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {SettingsService} from '../settings.service';
import {Currency, Settings} from '../settings.model';

@Component({
  selector: 'app-settings-form',
  templateUrl: './settings-form.component.html',
  styleUrls: ['./settings-form.component.scss']
})
export class SettingsFormComponent implements OnInit {

  settings: Settings;
  isLoading = false;
  form: FormGroup;
  CurrencyTypes = Currency;

  constructor(public settingsService: SettingsService) {
  }

  ngOnInit() {

    this.form = new FormGroup({
      defaultManDayCost: new FormControl(0, {validators: [Validators.required, Validators.min(1)]}),
      defaultChanceFactor: new FormControl(0, {validators: [Validators.required, Validators.min(0.1), Validators.max(1)]}),
      defaultRiskFactor: new FormControl(0, {validators: [Validators.required, Validators.min(1.1)]}),
      defaultLocsPerFunctionPoint: new FormControl(0, {validators: [Validators.required, Validators.min(0), Validators.max(5)]}),
      defaultCurrency: new FormControl('', {validators: [Validators.required]})
    });

    this.settingsService.getSettings().then((settings) => {
      this.settings = settings;
      this.form.setValue({
        defaultManDayCost: this.settings.defaultManDayCost,
        defaultChanceFactor: this.settings.defaultChanceFactor,
        defaultRiskFactor: this.settings.defaultRiskFactor,
        defaultLocsPerFunctionPoint: this.settings.defaultLocsPerFunctionPoint,
        defaultCurrency: this.settings.defaultCurrency
      });
    });
  }

  saveSettings() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    this.settings = this.form.getRawValue();
    this.settingsService.updateSettings(this.settings);
    this.form.reset();
  }

}
