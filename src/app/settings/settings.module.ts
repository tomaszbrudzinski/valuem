import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SettingsRouting} from './settings.routing';
import {ThemeModule} from '../@theme/theme.module';
import {SettingsFormComponent} from './settings-form/settings-form.component';

@NgModule({
  imports: [
    CommonModule,
    SettingsRouting,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    RouterModule
  ],
  declarations: [
    SettingsFormComponent,
  ]
})
export class SettingsModule {}

