export interface Setting {
  _id: string;
  name: string;
  value: any;
}

export interface Settings {
  defaultManDayCost: number;
  defaultChanceFactor: number;
  defaultRiskFactor: number;
  defaultLocsPerFunctionPoint: number;
  defaultCurrency: Currency;
}

export enum Currency {
  PLN = 'PLN',
  EUR = 'EUR',
  USD = 'USD',
  GBP = 'GBP'
}
