import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

import {Project, ValuationListElement} from './projects.model';
import {map} from 'rxjs/operators';

const BACKEND_URL = environment.apiUrl + '/projects/';

@Injectable({providedIn: 'root'})
export class ProjectsService {

  constructor(private http: HttpClient, private router: Router) {
  }

  private projects: Project[] = [];

  static getProjectContext(): string {
    return localStorage.getItem('currentProjectId');
  }

  static setProjectContext(clientId: string): string {
    localStorage.setItem('currentProjectId', clientId);
    return clientId;
  }

  getProject(id: string) {
    return this.http.get<{ _id: string, clientId: string; name: string, info: string, createDate: Date, updateDate: Date }>(BACKEND_URL + id);
  }

  getProjectValuations(projectId?: string) {
    let projectContext: string;
    if (projectId) {
      projectContext = projectId;
    } else {
      projectContext = ProjectsService.getProjectContext();
    }
    return this.http.get<ValuationListElement[]>(BACKEND_URL + projectContext + '/valuations')
      .pipe(map(valuationsData => {
        return valuationsData.map(valuation => {
          return {
            ...valuation,
            createDate: new Date(valuation.createDate),
            updateDate: new Date(valuation.updateDate)
          };
        });
      }));
  }

  addProject(projectClientId: string, projectName: string, projectInfo: string) {
    const projectData: Project = {
      _id: null,
      clientId: projectClientId,
      name: projectName,
      info: projectInfo,
      createDate: null,
      updateDate: null
    };
    this.http
      .post<{ message: string, project: Project }>(BACKEND_URL, projectData)
      .subscribe((response) => {
        this.router.navigate(['/projects/' + response.project._id + '/details']);
      });
  }

  updateProject(projectId: string, projectClientId: string, projectName: string, projectInfo: string) {
    const projectData: Project = {
      _id: projectId,
      clientId: projectClientId,
      name: projectName,
      info: projectInfo,
      createDate: null,
      updateDate: null
    };
    this.http.put(BACKEND_URL + projectId, projectData)
      .subscribe(response => {
        this.router.navigate(['/projects/' + projectId + '/details']);
      });
  }

  deleteProject(projectId: string) {
    return this.http.delete(BACKEND_URL + projectId);
  }
}
