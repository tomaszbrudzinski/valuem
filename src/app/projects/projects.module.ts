import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ProjectsRouting} from './projects.routing';
import {ThemeModule} from '../@theme/theme.module';
import {ProjectFormComponent} from './project-form/project-form.component';
import {ProjectDetailsComponent} from './project-details/project-details.component';
import {ProjectValuationsComponent} from './project-details/project-valuations/project-valuations.component';

@NgModule({
  imports: [
    CommonModule,
    ProjectsRouting,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    RouterModule
  ],
  declarations: [
    ProjectFormComponent,
    ProjectDetailsComponent,
    ProjectValuationsComponent
  ]
})
export class ProjectsModule {}

