import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

import {Client} from '../../clients/clients.model';
import {ClientsService} from '../../clients/clients.service';
import {Project, ValuationListElement} from '../projects.model';
import {ProjectsService} from '../projects.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  client: Client;
  project: Project;
  valuations: ValuationListElement[];
  isLoading = false;

  constructor(
    public clientsService: ClientsService,
    public projectsService: ProjectsService,
    private router: Router,
    public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('projectId')) {
        const projectId = paramMap.get('projectId');
        this.isLoading = true;
        this.projectsService.getProject(projectId).subscribe(projectData => {
          const clientId = ClientsService.getClientContext();
          this.clientsService.getClient(clientId).subscribe(clientData => {
            ProjectsService.setProjectContext(projectId);
            this.projectsService.getProjectValuations(projectId).subscribe((valuationsData) => {
              this.valuations = valuationsData;
            });
            this.client = clientData;
            this.isLoading = false;
          });
          this.isLoading = false;
          this.project = projectData;
        });
      } else {
        this.router.navigate(['/clients/' + this.client._id + '/details']);
      }
    });
  }

  deleteProject(projectId: string) {
    if (confirm('Are you sure you want to delete this project?')) {
      this.isLoading = true;
      this.projectsService.deleteProject(projectId).subscribe(() => {
        this.router.navigate(['/clients/' + this.client._id + '/details']);
      }, () => {
        this.isLoading = false;
      });
    }
  }

}
