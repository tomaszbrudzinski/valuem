import {Component, EventEmitter, Input, OnChanges, Output, ViewChild} from '@angular/core';

import {MatSort, MatTableDataSource} from '@angular/material';
import {Project, ValuationListElement} from '../../../projects/projects.model';

@Component({
  selector: 'app-client-projects',
  templateUrl: './project-valuations.component.html',
  styleUrls: ['./project-valuations.component.scss']
})
export class ProjectValuationsComponent implements OnChanges {


  sortableValuations: any;
  displayedColumns: string[] = ['name', 'createDate', 'updateDate'];
  @Input() valuations: ValuationListElement[];
  @Output() valuationClicked = new EventEmitter();
  @ViewChild(MatSort) sort: MatSort;

  ngOnChanges() {
    this.sortableValuations = new MatTableDataSource(this.valuations);
    this.sortableValuations.sort = this.sort;
  }

  showLoader() {
    this.valuationClicked.emit();
  }

}
