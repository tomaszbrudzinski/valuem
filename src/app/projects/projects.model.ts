export interface Project {
  _id: string;
  clientId: string;
  name: string;
  info: string;
  createDate: Date;
  updateDate: Date;
}

export interface ValuationListElement {
  _id: string;
  name: string;
  createDate: Date;
  updateDate: Date;
}
