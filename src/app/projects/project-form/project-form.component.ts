import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

import {ProjectsService} from '../projects.service';
import {Project} from '../projects.model';
import {ClientsService} from '../../clients/clients.service';
import {Client} from '../../clients/clients.model';

@Component({
  selector: 'app-client-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent implements OnInit {

  project: Project;
  client: Client;
  isLoading = false;
  form: FormGroup;
  private mode = 'create';
  private projectId: string;

  constructor(
    public projectsService: ProjectsService,
    private clientsService: ClientsService,
    public route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]}),
      info: new FormControl(null)
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {

      this.isLoading = true;
      const clientId = ClientsService.getClientContext();
      if (clientId && this.router.url.includes('create')) {
        this.mode = 'create';
        this.projectId = null;
        this.clientsService.getClient(clientId).subscribe(clientData => {
          this.client = clientData;
          this.isLoading = false;
          console.log(this.client);
        });

      } else if (paramMap.has('projectId')) {
        this.mode = 'edit';
        this.projectId = paramMap.get('projectId');
        this.projectsService.getProject(this.projectId).subscribe(projectData => {
          this.clientsService.getClient(clientId).subscribe(clientData => {
            this.client = clientData;
            this.project = projectData;
            this.form.setValue({
              name: this.project.name,
              info: this.project.info
            });
            this.isLoading = false;
          });
        });

      } else {
        this.router.navigate(['/']);
      }
    });
  }

  saveProject() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === 'create') {
      this.projectsService.addProject(
        this.client._id,
        this.form.value.name,
        this.form.value.info
      );
    } else {
      this.projectsService.updateProject(
        this.projectId,
        this.client._id,
        this.form.value.name,
        this.form.value.info,
      );
    }
    this.form.reset();
  }

}
