import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectFormComponent} from './project-form/project-form.component';
import {ProjectDetailsComponent} from './project-details/project-details.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full'},
  { path: ':projectId/details', component: ProjectDetailsComponent},
  { path: 'create', component: ProjectFormComponent },
  { path: ':projectId/edit', component: ProjectFormComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProjectsRouting {}
