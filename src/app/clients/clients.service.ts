import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

import {Client} from './clients.model';
import {map} from 'rxjs/operators';
import {Project} from '../projects/projects.model';

const BACKEND_URL = environment.apiUrl + '/clients/';

@Injectable({providedIn: 'root'})
export class ClientsService {

  constructor(private http: HttpClient, private router: Router) {
  }

  private clients: Client[] = [];
  private clientsUpdated = new Subject<{ clients: Client[], clientsCount: number, filtered: boolean }>();
  private currentClient: Client;

  static getClientContext(): string {
    return localStorage.getItem('currentClientId');
  }

  static setClientContext(clientId: string): string {
    localStorage.setItem('currentClientId', clientId);
    return clientId;
  }

  getClients(clientsPerPage: number, currentPage: number, name?: string) {
    let queryParams = `?pagesize=${clientsPerPage}&page=${currentPage}`;
    if (name) {
      queryParams += `&name=${name}`;
    }
    this.http
      .get<{ message: string, clients: any, clientsCount: number, filtered: boolean }>(BACKEND_URL + queryParams)
      .subscribe((clientData) => {
        this.clients = clientData.clients;
        this.clientsUpdated.next({
          clients: [...this.clients],
          clientsCount: clientData.clientsCount,
          filtered: clientData.filtered
        });
      });
  }

  getClientUpdateListener() {
    return this.clientsUpdated.asObservable();
  }

  getClient(id: string) {
    return this.http.get<{ _id: string, name: string, info: string }>(BACKEND_URL + id);
  }

  getClientProjects(clientId?: string) {
    let clientContext: string;
    if (clientId) {
      clientContext = clientId;
    } else {
      clientContext = ClientsService.getClientContext();
    }
    return this.http.get<Project[]>(BACKEND_URL + clientContext + '/projects')
      .pipe(map(projectData => {
        return projectData.map(project => {
            return {
              ...project,
              createDate: new Date(project.createDate),
              updateDate: new Date(project.updateDate)
            };
          });
      }));
  }

  addClient(clientName: string, clientInfo: string) {
    console.log(1);
    const clientData: Client = {
      _id: null,
      name: clientName,
      info: clientInfo
    };
    this.http
      .post<{ message: string, client: Client }>(BACKEND_URL, clientData)
      .subscribe((response) => {
        this.router.navigate(['/clients/' + response.client._id + '/details']);
      });
  }

  updateClient(clientId: string, clientName: string, clientInfo: string) {
    const clientData: Client = {
      _id: clientId,
      name: clientName,
      info: clientInfo
    };
    this.http.put(BACKEND_URL + clientId, clientData)
      .subscribe(response => {
        this.router.navigate(['/clients/' + clientId + '/details']);
      });
  }

  deleteClient(clientId: string) {
    return this.http.delete(BACKEND_URL + clientId);
  }
}
