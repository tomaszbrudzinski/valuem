import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {ActivatedRoute, ParamMap} from '@angular/router';

import {ClientsService} from '../clients.service';
import {Client} from '../clients.model';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss']
})
export class ClientFormComponent implements OnInit {

  client: Client;
  isLoading = false;
  form: FormGroup;
  private mode = 'create';
  private clientId: string;

  constructor(public clientsService: ClientsService, public route: ActivatedRoute) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]}),
      info: new FormControl(null, )
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('clientId')) {
        this.mode = 'edit';
        this.clientId = paramMap.get('clientId');
        this.isLoading = true;
        this.clientsService.getClient(this.clientId).subscribe(clientData => {
          this.isLoading = false;
          this.client = clientData;
          this.form.setValue({
            name: this.client.name,
            info: this.client.info
          });
        });
      } else {
        this.mode = 'create';
        this.clientId = null;
      }
    });
  }

  saveClient() {
    console.log('test');
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === 'create') {
      this.clientsService.addClient(
        this.form.value.name,
        this.form.value.info
      );
    } else {
      this.clientsService.updateClient(
        this.clientId,
        this.form.value.name,
        this.form.value.info,
      );
    }
    this.form.reset();
  }

}
