import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientListComponent} from './client-list/client-list.component';
import {ClientFormComponent} from './client-form/client-form.component';
import {ClientDetailsComponent} from './client-details/client-details.component';

const routes: Routes = [
  { path: '', component: ClientListComponent},
  { path: ':clientId/details', component: ClientDetailsComponent},
  { path: 'create', component: ClientFormComponent },
  { path: ':clientId/edit', component: ClientFormComponent },
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ClientsRouting {}
