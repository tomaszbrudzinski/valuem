import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

import {ClientsService} from '../clients.service';
import {Client} from '../clients.model';
import {Project} from '../../projects/projects.model';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {

  client: Client;
  projects: Project[];
  isLoading = false;

  constructor(
    public clientsService: ClientsService,
    private router: Router,
    public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('clientId')) {
        const clientId = paramMap.get('clientId');
        this.isLoading = true;
        this.clientsService.getClient(clientId).subscribe(clientData => {
          this.clientsService.getClientProjects(clientId).subscribe(projects => {
            this.isLoading = false;
            this.client = clientData;
            this.projects = projects;
            ClientsService.setClientContext(clientId);
          });
        });
      } else {
        this.router.navigate(['/clients/list']);
      }
    });
  }

  deleteClient(clientId: string) {
    if (confirm('Are you sure you want to delete this client?')) {
      this.isLoading = true;
      this.clientsService.deleteClient(clientId).subscribe(() => {
        this.router.navigate(['/']);
      }, () => {
        this.isLoading = false;
      });
    }
  }

}
