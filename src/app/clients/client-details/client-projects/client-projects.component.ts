import {Component, Input, OnChanges, ViewChild} from '@angular/core';

import {MatSort, MatTableDataSource} from '@angular/material';
import {Project} from '../../../projects/projects.model';

@Component({
  selector: 'app-client-projects',
  templateUrl: './client-projects.component.html',
  styleUrls: ['./client-projects.component.scss']
})
export class ClientProjectsComponent implements OnChanges {

  @Input() projects: Project[];
  @ViewChild(MatSort) sort: MatSort;
  sortableProjects: any;
  displayedColumns: string[] = ['name', 'createDate', 'updateDate'];

  ngOnChanges() {
    this.sortableProjects = new MatTableDataSource(this.projects);
    this.sortableProjects.sort = this.sort;
  }
}
