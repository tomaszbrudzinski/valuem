import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ClientsRouting} from './clients.routing';
import {ClientFormComponent} from './client-form/client-form.component';
import {ClientListComponent} from './client-list/client-list.component';
import {ThemeModule} from '../@theme/theme.module';
import {ClientDetailsComponent} from './client-details/client-details.component';
import {ClientProjectsComponent} from './client-details/client-projects/client-projects.component';

@NgModule({
  imports: [
    CommonModule,
    ClientsRouting,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    RouterModule
  ],
  declarations: [
    ClientListComponent,
    ClientFormComponent,
    ClientDetailsComponent,
    ClientProjectsComponent
  ]
})
export class ClientsModule {}

