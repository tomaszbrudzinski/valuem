import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {PageEvent} from '@angular/material';

import {Client} from '../clients.model';
import {ClientsService} from '../clients.service';
import {environment} from '../../../environments/environment';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit, OnDestroy {

  clients: Client[] = [];
  isLoading = false;
  clientsCount = 0;
  clientsPerPage = environment.itemsPerPage;
  currentPage = 1;
  pageSizeOptions = environment.pageSizeOptions;
  userId: string;
  filter = '';
  filtered = false;
  private clientsSubscription: Subscription;

  constructor(public clientsService: ClientsService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.clientsService.getClients(this.clientsPerPage, this.currentPage);
    this.clientsSubscription = this.clientsService.getClientUpdateListener()
      .subscribe((clientData: { clients: Client[], clientsCount: number, filtered: boolean}) => {
        this.isLoading = false;
        this.clients = clientData.clients;
        this.clientsCount = clientData.clientsCount;
        this.filtered = clientData.filtered;
      });
  }

  paginate(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.clientsPerPage = pageData.pageSize;
    this.clientsService.getClients(this.clientsPerPage, this.currentPage);
  }

  filterClients(form: NgForm) {
    this.clientsService.getClients(this.clientsPerPage, this.currentPage, form.value.filter);
  }

  clearFilter(form: NgForm) {
    form.setValue({filter: ''});
    this.clientsService.getClients(this.clientsPerPage, this.currentPage);
  }

  ngOnDestroy(): void {
    this.clientsSubscription.unsubscribe();
  }

}
