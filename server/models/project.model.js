const mongoose = require('mongoose');

const Valuation = require('./valuation.model');

const projectSchema =  mongoose.Schema({
  clientId: { type: mongoose.Schema.Types.ObjectId, ref: "Client", required: true },
  name: { type: String, required: true },
  info: { type: String },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true }
});

module.exports = mongoose.model('Project', projectSchema);
