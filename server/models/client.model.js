const mongoose = require('mongoose');

const clientSchema =  mongoose.Schema({
  name: { type: String, required: true },
  info: { type: String },
});

module.exports = mongoose.model('Client', clientSchema);
