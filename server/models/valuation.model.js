const mongoose = require('mongoose');

const generalSystemCharacteristicsSchema = mongoose.Schema({
  dataCommunication: { type: Number },
  distributedDataProcessing: { type: Number },
  performance: { type: Number },
  heavilyUsedConfiguration: { type: Number },
  transactionRole: { type: Number },
  onlineDataEntry: { type: Number },
  endUserEfficiency: { type: Number },
  onlineUpdate: { type: Number },
  complexProcessing: { type: Number },
  reusability: { type: Number },
  installationEase: { type: Number },
  operationalEase: { type: Number },
  multipleSites: { type: Number },
  facilitateChange: { type: Number },
});

const milestoneSchema = mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  duration: { type: Number},
  budget: { type: Number }
});

const taskSchema = mongoose.Schema({
  name: { type: String, required: true },
  milestone: { type: Number, required: true },
  complexity: { type: String },
  inputs: { type: Number },
  outputs: { type: Number },
  inquiries: { type: Number },
  files: { type: Number },
  interfaces: { type: Number },
  workloadMin: { type: Number },
  workloadMax: { type: Number }
});

const costSchema = mongoose.Schema({
  name: { type: String, required: true },
  milestone: { type: Number, required: true},
  value: { type: Number, required: true}
});

const checkpointSchema = mongoose.Schema({
  date: { type: Date, required: true },
  bcwp: { type: Number, required: true},
  acwp: { type: Number, required: true}
});

const valuationSchema =  mongoose.Schema({
  projectId: { type: mongoose.Schema.Types.ObjectId, ref: "Project", required: true },
  name: { type: String, required: true },
  type: { type: String, required: true},
  info: { type: String },
  currency: { type: String, required: true},
  complexity: { type: String },
  targetBudget: { type: Number },
  manDayCost: { type: Number, required: true },
  chanceFactor: { type: Number },
  riskFactor: { type: Number },
  locsPerFunctionPoint: { type: Number },
  generalSystemCharacteristics: generalSystemCharacteristicsSchema,
  startDate: { type: Date, required: true },
  createDate: { type: Date, required: true },
  updateDate: { type: Date, required: true },
  costs: [costSchema],
  milestones: [milestoneSchema],
  tasks: [taskSchema],
  checkpoints: [checkpointSchema]
});

module.exports = mongoose.model('Valuation', valuationSchema);
