const seeder = require('mongoose-seed');

const mongoose = require('mongoose');

// const Setting = require('../models/setting.model');

const data = [
  {
    model: 'Setting',
    documents: [
      {
        name: 'defaultManDayCost',
        value: 1200
      },
      {
        name: 'defaultChanceFactor',
        value: 0.9
      },
      {
        name: 'defaultRiskFactor',
        value: 1.5
      },
      {
        name: 'defaultLocsPerFunctionPoint',
        value: 55
      },
      {
        name: 'defaultCurrency',
        value: 'PLN'
      }
    ]
  }
];

exports.seedSettings = () => {

  let mongodbCredentials = '';

  if (process.env.MONGO_USER && process.env.MONGO_PASS) {
    mongodbCredentials = process.env.MONGO_USER + ':' + process.env.MONGO_PASS + '@';
  }

  seeder.connect('mongodb://' + mongodbCredentials + process.env.MONGO_HOST + '/' + process.env.MONGO_DB, function() {

    // Load Mongoose models
    seeder.loadModels([
      __dirname + '/../models/setting.model.js'
    ]);

    // Clear specified collections
    seeder.clearModels(['Setting'], function() {

      // Callback to populate DB once collections have been cleared
      seeder.populateModels(data, function() {
        seeder.disconnect();
      });

    });
  });
}
