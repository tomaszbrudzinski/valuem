const express = require('express');

const SettingsController = require('../controllers/settings.controller');

const router = express.Router();

router.put('', SettingsController.updateSettings);

router.get('', SettingsController.getSettings);

module.exports = router;
