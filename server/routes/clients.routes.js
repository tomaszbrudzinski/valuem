const express = require('express');

const ClientsController = require('../controllers/clients.controller');

const router = express.Router();

router.post('', ClientsController.createClient);

router.put('/:clientId', ClientsController.updateClient);

router.get('', ClientsController.getClients);

router.get('/:clientId', ClientsController.getClient);
router.get('/:clientId/projects', ClientsController.getClientProjects);

router.delete('/:clientId', ClientsController.deleteClient);

module.exports = router;
