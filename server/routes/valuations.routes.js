const express = require('express');

const ValuationsController = require('../controllers/valuations.controller');

const router = express.Router();

router.post('', ValuationsController.createValuation);

router.put('/:id', ValuationsController.updateValuation);

router.get('/:id', ValuationsController.getValuation);

router.delete('/:id', ValuationsController.deleteValuation);

module.exports = router;
