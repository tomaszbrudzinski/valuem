const express = require('express');

const ProjectsController = require('../controllers/projects.controller');

const router = express.Router();

router.post('', ProjectsController.createProject);

router.put('/:id', ProjectsController.updateProject);

router.get('/:id', ProjectsController.getProject);

router.delete('/:id', ProjectsController.deleteProject);
router.get('/:projectId/valuations', ProjectsController.getProjectValuations);

module.exports = router;
