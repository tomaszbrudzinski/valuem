const Project = require('../models/project.model');
const Valuation = require('../models/valuation.model');

exports.createProject = (req, res, next) => {
  const now = Date.now();
  const project = new Project({
    clientId: req.body.clientId,
    name: req.body.name,
    info: req.body.info,
    createDate: now,
    updateDate: now,
  });
  project.save().then(createdProject => {
    res.status(201).json({
      message: 'Project added successfully',
      project: {
        ...createdProject._doc
      }
    });
  })
    .catch(error => {
      console.error(error);
      res.status(500).json({
        message: 'Creating a project failed'
      });
    });
};

exports.updateProject = (req, res, next) => {
  const now = Date.now();
  const project = new Project({
    _id: req.body._id,
    name: req.body.name,
    info: req.body.info,
    updateDate: now,
  });
  Project.updateOne({_id: req.body._id}, project).then(result => {
    res.status(200).json({message: 'Update successfull'});
  })
    .catch(error => {
      console.log(error);
      res.status(500).json({
        message: 'Couldn\'t update project details'
      })
    });
};

exports.getProject = (req, res, next) => {
  Project.findById(req.params.id).then(project => {
    if (project) {
      res.status(200).json(project);
    } else {
      res.status(404).json({message: 'Project not found'});
    }
  })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching project failed'
      })
    });
};

exports.getProjectValuations = (req, res, next) => {
  Valuation.find({projectId: req.params.projectId}, 'name createDate updateDate').then(valuations => {
    if (valuations) {
      res.status(200).json(valuations);
    } else {
      res.status(404).json({message: 'Valuations not found'});
    }
  })
  .catch(error => {
    console.error(error);
    res.status(500).json({
      message: 'Fetching valuations failed'
    })
  });
};

exports.deleteProject = (req, res, next) => {
  Project.deleteOne({_id: req.params.id}).then(result => {
    Valuation.deleteMany({projectId: req.params.id}).then(()=>{
      res.status(200).json({message: 'Project deleted!'});
    })
      .catch(error => {
        res.status(500).json({
          message: 'Deleting project failed'
        })
      });;
  })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting project failed'
      })
    });
};
