const Setting = require('../models/setting.model');

exports.updateSettings = (req, res, next) => {
  for (i in req.body) {
    Setting.updateOne({_id: req.body[i]._id }, req.body[i]).then().catch(error => {
      console.error(error);
      res.status(500).json({
        message: 'Couldn\'t update client details'
      })
    });
  }

  res.status(200).json({
    message: 'Update successfull',
  });

};

exports.getSettings = (req, res, next) => {
  Setting.find({}).then(settings => {
    if (settings) {
      res.status(200).json(settings);
    } else {
      res.status(404).json({message: 'Settings not found'});
    }
  })
  .catch(error => {
    res.status(500).json({
      message: 'Fetching settings failed'
    })
  });
};
