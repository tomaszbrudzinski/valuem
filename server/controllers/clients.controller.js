const Client = require('../models/client.model');
const Project = require('../models/project.model');
const Valuation = require('../models/valuation.model');

exports.createClient = (req, res, next) => {
  const client = new Client({
    name: req.body.name,
    info: req.body.info
  });
  client.save().then(createdClient => {
    res.status(201).json({
      message: 'Client added successfully',
      client: {
        ...createdClient._doc
      }
    });
  })
  .catch(error => {
    console.error(error);
    res.status(500).json({
      message: 'Creating a client failed'
    });
  });
};

exports.updateClient = (req, res, next) => {
  const client = new Client({
    _id: req.body._id,
    name: req.body.name,
    info: req.body.info
  });
  Client.updateOne({_id: req.body._id }, client).then(result => {
    res.status(200).json({
      message: 'Update successfull',
    });
  })
  .catch(error => {
    console.error(error);
    res.status(500).json({
      message: 'Couldn\'t update client details'
    })
  });
};

exports.getClients = (req, res, next) => {

  const name = req.query.name;
  let nameFilter = null;
  if (name) {
    nameFilter = { name: new RegExp(name, 'i')};
  }

  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const clientQuery = Client.find(nameFilter, null, {sort: {name: 1}});
  let fetchedClients;
  if (pageSize && currentPage) {
    clientQuery
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize);
  }

  clientQuery
    .then(documents => {
      fetchedClients = documents;
      return Client.countDocuments();
    })
    .then(count => {
      res.status(200).json({
        message: 'Clients fetched successfully!',
        clients: fetchedClients,
        clientsCount: count,
        filtered: !!name,
      });
    })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching clients failed'
      })
    });
};

exports.getClient = (req, res, next) => {
  Client.findById(req.params.clientId).then(client => {
    if (client) {
      res.status(200).json(client);
    } else {
      res.status(404).json({message: 'Client not found'});
    }
  })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching client failed'
      })
    });
};

exports.getClientProjects = (req, res, next) => {
  Project.find({clientId: req.params.clientId}).then(projects => {
    if (projects) {
      res.status(200).json(projects);
    } else {
      res.status(404).json({message: 'Projects not found'});
    }
  })
    .catch(error => {
      console.error(error);
      res.status(500).json({
        message: 'Fetching projects failed'
      })
    });
};

exports.deleteClient = (req, res, next) => {
  let errorOccured = false;

  Client.deleteOne({_id: req.params.clientId}).then(() => {
    Project.find({clientId: req.params.clientId}).then(relatedProjects => {
      for (project of relatedProjects) {
        Project.deleteMany({_id: project._id}).then(() => {
          Valuation.deleteMany({projectId: project._id}).then()
          .catch(error => {
            errorOccured = true;
            console.error(error);
          });
        })
        .catch(error => {
          errorOccured = true;
          console.error(error);
        });
      }
    })
    .catch(error => {
      errorOccured = true;
      console.error(error);
    });
  })
  .catch(error => {
    errorOccured = true;
    console.error(error);
  });
  if(!errorOccured){
    res.status(200).json({message: 'Client deleted!'});
  } else {
    res.status(500).json({
      message: 'Deleting client failed'
    });
  }
};
