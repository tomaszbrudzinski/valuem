const Valuation = require('../models/valuation.model');

exports.createValuation = (req, res, next) => {
  const now = Date.now();
  const valuation = new Valuation({
    ...req.body,
    createDate: now,
    updateDate: now
  });
  valuation.save().then(createdValuation => {
    res.status(201).json({
      message: 'Valuation added successfully',
      project: {
        ...createdValuation._doc
      }
    });
  })
  .catch(error => {
    console.error(error);
    res.status(500).json({
      message: 'Creating a valuation failed'
    });
  });
};

exports.updateValuation = (req, res, next) => {
  const now = Date.now();
  const valuation = new Valuation({
    ...req.body,
    updateDate: now,
  });
  Valuation.updateOne({_id: req.body._id}, valuation).then(result => {
    res.status(200).json({message: 'Update successfull'});
  })
  .catch(error => {
    console.log(error);
    res.status(500).json({
      message: 'Couldn\'t update project details'
    })
  });
};

exports.getValuation = (req, res, next) => {
  Valuation.findById(req.params.id).then(valuation => {
    if (valuation) {
      res.status(200).json(valuation);
    } else {
      res.status(404).json({message: 'Valuation not found'});
    }
  })
    .catch(error => {
      res.status(500).json({
        message: 'Fetching valuation failed'
      })
    });
};

exports.deleteValuation = (req, res, next) => {
  Valuation.deleteOne({_id: req.params.id}).then(result => {
    res.status(200).json({message: 'Valuation deleted!'});
  })
    .catch(error => {
      res.status(500).json({
        message: 'Deleting valuation failed'
      })
    });
};
