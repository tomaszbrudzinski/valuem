const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const clientsRoutes = require('./routes/clients.routes');
const projectsRoutes = require('./routes/projects.routes');
const valuationsRoutes = require('./routes/valuations.routes');
const settingsRoutes = require('./routes/settings.routes');

const Setting = require('./models/setting.model')
const settingsSeeder = require('./seeds/settings.seed');

const app = express();

let mongodbCredentials = '';

if (process.env.MONGO_USER && process.env.MONGO_PASS) {
  mongodbCredentials = process.env.MONGO_USER + ':' + process.env.MONGO_PASS + '@';
}

mongoose.connect('mongodb://' + mongodbCredentials + process.env.MONGO_HOST + '/' + process.env.MONGO_DB + '?retryWrites=true', {useNewUrlParser: true})
  .then(() => {
    console.log('Connected to database');
    Setting.countDocuments(null, (error, count) => {
      if(count <= 0) {
        console.log('Settings are missing - Initializing default settings...')
        settingsSeeder.seedSettings();
      } else {
        console.log('Settings data checked')
      }

    });
  })
  .catch(error => {
    console.log(error);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/images', express.static(path.join('images')));

app.use((req, res, next) => {
  res.setHeader(
    'Access-Control-Allow-Origin',
    '*'
  );
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, PUT, DELETE, OPTIONS'
  );
  next();
});

app.use('/api/clients', clientsRoutes);
app.use('/api/projects', projectsRoutes);
app.use('/api/valuations', valuationsRoutes);
app.use('/api/settings', settingsRoutes);

module.exports = app;
